from flask import Flask, Markup, flash, redirect, render_template, request, session, abort
import chess
import chess.svg

app = Flask(__name__)

@app.route("/")
def index():
    return "Index!"

@app.route("/svg")
def hello():
    svgObj = svgBoard()  
    #print(svgObj)
    return render_template('hello.html', name=Markup(svgObj))

def svgBoard():
    board = chess.Board("8/8/8/8/4N3/8/8/8 w - - 0 1")
    squares = board.attacks(chess.E4)
    return chess.svg.board(board, squares=squares, size=350)

def svgPiece():
    return chess.svg.piece(chess.Piece.from_symbol("R"))  

if __name__ == "__main__":
    app.run()


#from flask import Flask
#app = Flask(__name__)
#
#@app.route("/")
#def hello():
#    return "Hello World!"
#
#if __name__ == "__main__":
#    app.run()
